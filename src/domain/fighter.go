package domain

// Fighter структура отображающая данные бойца
type Fighter struct {
	//Name Имя бойца на русском языке
	Name string `json:"name"`

	//Fight Бои в которых принял участие спортсмен
	Fight []Fight `json:"fight"`
}
