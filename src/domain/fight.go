package domain

import "time"

//Fight Сушность отражающая характеристики боя
type Fight struct {
	//Card Кард в составе которого состоялся поединок
	Card Card

	//Decision Результат с которым закончился поединок
	Decision Decision

	//Opponents Бойцы
	Opponents [2]Fighter

	//Division Дивизион в котором проходил поединок
	Division Division
}

// Decision результат боя
type Decision struct {
	//Round Раунд в котором закончился поединок
	Round int

	//Time Время остановки (в секундах)
	Time int

	//Type Результат которым закончился поединок
	Type DecisionType
}

//DecisionType Тип решений которым закончился бой
type DecisionType int

const (
	//KO Нокаут
	KO DecisionType = iota
	//TKO Технический нокаут
	TKO
	//SUB Болевой или удушающий прием
	SUB
	//DEC Решение судей
	DEC
	//NC Результат боя не действителен
	NC
)

// Card характеристики карда
type Card struct {
	//Title Заголовок карда (Fight night или номерной UFC)
	Title string

	//Number Номер карда
	Number int

	//Date Дата мероприятия
	Date time.Time
}
