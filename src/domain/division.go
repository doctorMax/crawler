package domain

//Division Сущность характерихующая дивизион
type Division struct {
	//Name Название дивизиона
	Name string

	//Weight Лимит веса
	Weight WeightClass

	//TopListLength Количество рейтинговых бойцов (топ-15)
	TopListLength int
}

// WeightClass весовая категория
type WeightClass struct {
	//Kilo Лимит в килограммах
	Kilo int
	//Pound Лимит в фунтах
	Pound int
}
